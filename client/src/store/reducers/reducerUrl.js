import {CREATE_URL_SUCCESS} from "../actions/actionUrl";

const initialState = {
    link: ''
};

const reducerUrl = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_URL_SUCCESS:
            return {...state, link: action.link};
        default:
            return state;
    }
};

export default reducerUrl;