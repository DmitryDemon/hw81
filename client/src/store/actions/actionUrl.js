import axios from '../../axios-api';

export const CREATE_URL_SUCCESS = 'CREATE_URL_SUCCESS';

export const createUrlSuccess = link => {return {type: CREATE_URL_SUCCESS,link};
};

export const postUrl = link => {
    return dispatch => {
        return axios.post('/', link).then(
            response => {
                dispatch(createUrlSuccess(response.data))
            }
        );
    };
};