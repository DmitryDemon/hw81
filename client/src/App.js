import React, { Component } from 'react';

import './App.css';
import {Button, Col, Form, FormGroup, Input} from "reactstrap";
import {postUrl} from "./store/actions/actionUrl";
import {connect} from "react-redux";
import {apiURL} from "./constants";

class App extends Component {
    state = {
        longUrl:''
    };


    submitFormHandler = event => {
        event.preventDefault();
        const longUrl = this.state;
        this.props.postUrl(longUrl);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

  render() {
      return (
      <div className="App">
        <h1>Shorten your link</h1>
          <Form onSubmit={this.submitFormHandler}>
            <FormGroup row>
              <Col sm={12}>
                <Input
                    type="text" required
                    name="longUrl" id="longUrl"
                    placeholder="Enter URL here"
                    value={this.state.longUrl}
                    onChange={this.inputChangeHandler}
                />
              </Col>
            </FormGroup>
              <FormGroup row>
                  <Col sm={{offset:2, size: 10}}>
                      <Button type="submit" color="primary">Shorten !</Button>
                  </Col>
              </FormGroup>
          </Form>
          <h2> Your link now looks like this</h2>
          <a href={apiURL+'/'+this.props.link.link}>
              {this.props.link.link ==='' ? null: apiURL+'/'+this.props.link.link}
          </a>


      </div>
    );
  }
}

const mapStateToProps = state => {
    return {
        link: state.link,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        postUrl: link => dispatch(postUrl(link))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
